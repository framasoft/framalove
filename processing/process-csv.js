let fs = require('fs');
let Papa = require('papaparse');

let buffer = fs.readFileSync("./framalove.csv");
let content = buffer.toString();

let parsed = Papa.parse(content, { delimiter: "," });

for (let err of parsed.errors) {
    console.log("error when parsing:", err);
}

let lines = parsed.data.filter(line => typeof line[2] !== 'undefined' && line[2].trim() === "<3").map(line => [line[0]]);
let csvLines = Papa.unparse(lines, { delimiter: ",", newline: '\n' });

fs.writeFileSync("./framaloveactually.csv", csvLines);
console.log("My work here is done ⬇🎤");
