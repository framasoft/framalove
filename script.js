function chooseOne(lines) {
    let line = lines[Math.random() * lines.length | 0][0];

    console.log("J'ai choisi", line);
    document.getElementById("target").textContent = line;
}

(async function() {
    document.addEventListener('DOMContentLoaded', () => {
        document.getElementById('gimme-more').addEventListener('click', (ev) => {
            if (ev) {
                ev.preventDefault();
            }
            chooseOne(lines);
        });
    });

    let bytes = await fetch("framaloveactually.csv");
    let content = await bytes.text();

    let parsed = Papa.parse(content, {
        delimiter: ","
    });

    if (typeof parsed.errors !== 'undefined' && parsed.errors.length > 0) {
        console.error("Erreurs pendant l'analyse syntaxique du CSV:", parsed.errors);
    }

    let lines = parsed.data;

    // Remove last line, if it is empty.
    if (lines[lines.length - 1][0].trim().length === 0) {
        lines.pop();
    }

    chooseOne(lines);
})().catch(err => {
    console.error("Erreur:", err);;
});
